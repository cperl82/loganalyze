#!/usr/bin/env python

# vim: et sw=4 ts=4 softtabstop=4 foldmethod=expr tw=100

import os
import sys
import re
import glob
import unittest

from datetime import datetime, timedelta

# TODO: Figure out a good way to vary time periods

def dt(datetime_str):
    """
    Takes a string like '2011-10-24 00:00:00' and convert it into a datetime object
    """
    return datetime(*[int(x) for x in re.split('[-: ]', datetime_str)])

class LogFileTester(unittest.TestCase):
    def setUp(self):
        self._devnull = open(os.devnull, 'w')
        self.log_analyzer = LogAnalyzer(display_obj=self._devnull)
        self._setup_log_paths()
        for path in self.logs:
            self.log_analyzer.parse_logfile(path)

    def tearDown(self):
        self._devnull.close()

class October24th2011_AppServer06_Port8131(LogFileTester):
    def _setup_log_paths(self):
        self.logs = glob.glob(
                     os.path.join(
                       os.path.dirname(__file__), "logs", "server-8131", "*2011-10-24*"))

    def test_LogFileLength(self):
        self.assertEqual(len(self.log_analyzer.log_data), 245686)

    def test_HitsPerUrl(self):
        expected_data = {
            'www.nhl.com/ice/m_draft.htm':  17,
            'www.nhl.com/ice/m_awards.htm': 17,
            'www.nhl.com/ice/m_events.htm': 50,
            'www.nhl.com/ice/m_photos.htm': 58,
            'www.nhl.com/ice/m_stats.htm':  828,
            'www.nhl.com/ice/m_teams.htm':  833,
            'www.nhl.com/ice/m_scores.htm': 21967,
        }
        self.log_analyzer.do_cmd("topurls --filter '/ice/m_......?\.htm$'")
        self.assertEqual(self.log_analyzer._hpu.aggr_data, expected_data)

    def test_HitsPerIP(self):
        expected_data = {
            '208.92.134.5':   1,
            '208.92.17.243':  1,
            '208.92.19.165':  1,
            '208.92.136.50':  1,
            '208.92.241.25':  1,
            '208.92.228.62':  1,
            '208.92.18.107':  1,
            '208.92.59.110':  1,
            '208.92.59.206':  1,
            '208.92.139.246': 1,
            '208.92.224.27':  1,
            '208.92.240.21':  1,
            '208.92.19.114':  1,
            '208.92.36.12':   36,
            '208.92.36.18':   1793,
        }
        self.log_analyzer.do_cmd("toptalkers --filter '^208\.92\.'")
        self.assertEqual(self.log_analyzer._hpi.aggr_data, expected_data)

    def test_HitsPerUrlPerUnitTime3600(self):
        expected_data = { 
            'www.nhl.com/ice/m_home.htm': { 'hits':             20645,
                (dt('2011-10-24 00:00:00'), dt('2011-10-24 00:59:59')): 1775,
                (dt('2011-10-24 01:00:00'), dt('2011-10-24 01:59:59')): 1236,
                (dt('2011-10-24 02:00:00'), dt('2011-10-24 02:59:59')): 355,
                (dt('2011-10-24 03:00:00'), dt('2011-10-24 03:59:59')): 212,
                (dt('2011-10-24 04:00:00'), dt('2011-10-24 04:59:59')): 239,
                (dt('2011-10-24 05:00:00'), dt('2011-10-24 05:59:59')): 254,
                (dt('2011-10-24 06:00:00'), dt('2011-10-24 06:59:59')): 297,
                (dt('2011-10-24 07:00:00'), dt('2011-10-24 07:59:59')): 327,
                (dt('2011-10-24 08:00:00'), dt('2011-10-24 08:59:59')): 383,
                (dt('2011-10-24 09:00:00'), dt('2011-10-24 09:59:59')): 463,
                (dt('2011-10-24 10:00:00'), dt('2011-10-24 10:59:59')): 484,
                (dt('2011-10-24 11:00:00'), dt('2011-10-24 11:59:59')): 514,
                (dt('2011-10-24 12:00:00'), dt('2011-10-24 12:59:59')): 585,
                (dt('2011-10-24 13:00:00'), dt('2011-10-24 13:59:59')): 494,
                (dt('2011-10-24 14:00:00'), dt('2011-10-24 14:59:59')): 582,
                (dt('2011-10-24 15:00:00'), dt('2011-10-24 15:59:59')): 559,
                (dt('2011-10-24 16:00:00'), dt('2011-10-24 16:59:59')): 585,
                (dt('2011-10-24 17:00:00'), dt('2011-10-24 17:59:59')): 709,
                (dt('2011-10-24 18:00:00'), dt('2011-10-24 18:59:59')): 815,
                (dt('2011-10-24 19:00:00'), dt('2011-10-24 19:59:59')): 1882,
                (dt('2011-10-24 20:00:00'), dt('2011-10-24 20:59:59')): 2292,
                (dt('2011-10-24 21:00:00'), dt('2011-10-24 21:59:59')): 2695,
                (dt('2011-10-24 22:00:00'), dt('2011-10-24 22:59:59')): 1858,
                (dt('2011-10-24 23:00:00'), dt('2011-10-24 23:59:59')): 1050,
            }
        }
        self.log_analyzer.do_cmd("url-hits-per-time --filter '/ice/m_home\.htm$'")
        self.assertEqual(self.log_analyzer._hpuput.aggr_data, expected_data)

    def test_HitsPerUrlPerUnitTime8500(self):
        expected_data = {
            'www.nhl.com/ice/m_menu.htm': { 'hits': 3792,
                (dt('2011-10-24 00:00:00'), dt('2011-10-24 02:21:39')): 220,
                (dt('2011-10-24 02:21:40'), dt('2011-10-24 04:43:19')): 126,
                (dt('2011-10-24 04:43:20'), dt('2011-10-24 07:04:59')): 114,
                (dt('2011-10-24 07:05:00'), dt('2011-10-24 09:26:39')): 152,
                (dt('2011-10-24 09:26:40'), dt('2011-10-24 11:48:19')): 208,
                (dt('2011-10-24 11:48:20'), dt('2011-10-24 14:09:59')): 222,
                (dt('2011-10-24 14:10:00'), dt('2011-10-24 16:31:39')): 232,
                (dt('2011-10-24 16:31:40'), dt('2011-10-24 18:53:19')): 329,
                (dt('2011-10-24 18:53:20'), dt('2011-10-24 21:14:59')): 1099,
                (dt('2011-10-24 21:15:00'), dt('2011-10-24 23:36:39')): 1014,
                (dt('2011-10-24 23:36:40'), dt('2011-10-24 23:59:59')): 76,
            }
        }
        self.log_analyzer.do_cmd("url-hits-per-time --filter '/ice/m_menu\.htm$' --step 8500")
        self.assertEqual(self.log_analyzer._hpuput.aggr_data, expected_data)

    def test_HitsPerIPPerUnitTime3600(self):
        expected_data = {
            '67.195.112.115': { 'hits': 2795,
                (dt('2011-10-24 00:00:00'), dt('2011-10-24 00:59:59')): 100,
                (dt('2011-10-24 01:00:00'), dt('2011-10-24 01:59:59')): 192,
                (dt('2011-10-24 02:00:00'), dt('2011-10-24 02:59:59')): 103,
                (dt('2011-10-24 03:00:00'), dt('2011-10-24 03:59:59')): 149,
                (dt('2011-10-24 04:00:00'), dt('2011-10-24 04:59:59')): 152,
                (dt('2011-10-24 05:00:00'), dt('2011-10-24 05:59:59')): 186,
                (dt('2011-10-24 06:00:00'), dt('2011-10-24 06:59:59')): 176,
                (dt('2011-10-24 07:00:00'), dt('2011-10-24 07:59:59')): 250,
                (dt('2011-10-24 08:00:00'), dt('2011-10-24 08:59:59')): 195,
                (dt('2011-10-24 09:00:00'), dt('2011-10-24 09:59:59')): 166,
                (dt('2011-10-24 10:00:00'), dt('2011-10-24 10:59:59')): 124,
                (dt('2011-10-24 11:00:00'), dt('2011-10-24 11:59:59')): 43,
                (dt('2011-10-24 12:00:00'), dt('2011-10-24 12:59:59')): 45,
                (dt('2011-10-24 13:00:00'), dt('2011-10-24 13:59:59')): 116,
                (dt('2011-10-24 14:00:00'), dt('2011-10-24 14:59:59')): 114,
                (dt('2011-10-24 15:00:00'), dt('2011-10-24 15:59:59')): 88,
                (dt('2011-10-24 16:00:00'), dt('2011-10-24 16:59:59')): 54,
                (dt('2011-10-24 17:00:00'), dt('2011-10-24 17:59:59')): 41,
                (dt('2011-10-24 18:00:00'), dt('2011-10-24 18:59:59')): 58,
                (dt('2011-10-24 19:00:00'), dt('2011-10-24 19:59:59')): 65,
                (dt('2011-10-24 20:00:00'), dt('2011-10-24 20:59:59')): 52,
                (dt('2011-10-24 21:00:00'), dt('2011-10-24 21:59:59')): 46,
                (dt('2011-10-24 22:00:00'), dt('2011-10-24 22:59:59')): 48,
                (dt('2011-10-24 23:00:00'), dt('2011-10-24 23:59:59')): 232,
            }
        }
        self.log_analyzer.do_cmd("ip-hits-per-time --filter ^67.195.112.115$")
        self.assertEqual(self.log_analyzer._hpiput.aggr_data, expected_data)

    def test_HitsPerIPPerUnitTime3598(self):
        expected_data = {
            '66.249.72.196': { 'hits': 3231,
                (dt('2011-10-24 00:00:00'), dt('2011-10-24 00:59:58')): 74,
                (dt('2011-10-24 00:59:59'), dt('2011-10-24 01:59:57')): 92,
                (dt('2011-10-24 01:59:58'), dt('2011-10-24 02:59:56')): 143,
                (dt('2011-10-24 02:59:57'), dt('2011-10-24 03:59:55')): 94,
                (dt('2011-10-24 03:59:56'), dt('2011-10-24 04:59:54')): 135,
                (dt('2011-10-24 04:59:55'), dt('2011-10-24 05:59:53')): 161,
                (dt('2011-10-24 05:59:54'), dt('2011-10-24 06:59:52')): 153,
                (dt('2011-10-24 06:59:53'), dt('2011-10-24 07:59:51')): 166,
                (dt('2011-10-24 07:59:52'), dt('2011-10-24 08:59:50')): 185,
                (dt('2011-10-24 08:59:51'), dt('2011-10-24 09:59:49')): 181,
                (dt('2011-10-24 09:59:50'), dt('2011-10-24 10:59:48')): 202,
                (dt('2011-10-24 10:59:49'), dt('2011-10-24 11:59:47')): 116,
                (dt('2011-10-24 11:59:48'), dt('2011-10-24 12:59:46')): 209,
                (dt('2011-10-24 12:59:47'), dt('2011-10-24 13:59:45')): 155,
                (dt('2011-10-24 13:59:46'), dt('2011-10-24 14:59:44')): 166,
                (dt('2011-10-24 14:59:45'), dt('2011-10-24 15:59:43')): 122,
                (dt('2011-10-24 15:59:44'), dt('2011-10-24 16:59:42')): 125,
                (dt('2011-10-24 16:59:43'), dt('2011-10-24 17:59:41')): 97,
                (dt('2011-10-24 17:59:42'), dt('2011-10-24 18:59:40')): 128,
                (dt('2011-10-24 18:59:41'), dt('2011-10-24 19:59:39')): 111,
                (dt('2011-10-24 19:59:40'), dt('2011-10-24 20:59:38')): 110,
                (dt('2011-10-24 20:59:39'), dt('2011-10-24 21:59:37')): 118,
                (dt('2011-10-24 21:59:38'), dt('2011-10-24 22:59:36')): 113,
                (dt('2011-10-24 22:59:37'), dt('2011-10-24 23:59:35')): 74,
                (dt('2011-10-24 23:59:36'), dt('2011-10-24 23:59:59')): 1,
            }
        }
        self.log_analyzer.do_cmd("ip-hits-per-time --filter ^66.249.72.196$ --step 3599")
        self.assertEqual(self.log_analyzer._hpiput.aggr_data, expected_data)

    def test_HitsPerUrlByTimeToRenderLinear500(self):
        expected_data = {
            'www.nhl.com/ice/search.htm': { 'hits': 3044,
                (0, 500):     3040,
                (500, 1000):  2,
                (1000, 1500): 1,
                (2000, 2500): 1,
            }
        }
        self.log_analyzer.do_cmd("url-time-to-render --filter '^www.nhl.com/ice/search.htm$'")
        self.assertEqual(self.log_analyzer._hpubttrl.aggr_data, expected_data)

    def test_HitsPerUrlByTimeToRenderLinear50(self):
        expected_data = {
            'www.nhl.com/ice/m_teams.htm': { 'hits': 833,
                (0, 10):   590,
                (10, 20):  70,
                (20, 30):  151,
                (30, 40):  11,
                (40, 50):  1,
                (50, 60):  1,
                (60, 70):  7,
                (70, 80):  1,
                (90, 100): 1,
            }
        }
        self.log_analyzer.do_cmd("url-time-to-render --filter '^www.nhl.com/ice/m_teams.htm$' --step 10")
        self.assertEqual(self.log_analyzer._hpubttrl.aggr_data, expected_data)

    def test_HitsPerIPByTimeToRenderLinear500(self):
        expected_data = {
            '194.158.197.10': { 'hits': 2095,
                (0, 500):     2092,
                (3000, 3500): 1,
                (4000, 4500): 2,
            }
        }
        self.log_analyzer.do_cmd("ip-time-to-render --filter '^194.158.197.10$'")
        self.assertEqual(self.log_analyzer._hpibttrl.aggr_data, expected_data)

    def test_HitsPerIPByTimeToRenderLinear10(self):
        expected_data = {
            '68.171.231.81': { 'hits': 1460,
                (0, 10):      203,
                (10, 20):     163,
                (20, 30):     287,
                (30, 40):     223,
                (40, 50):     177,
                (50, 60):     116,
                (60, 70):     82,
                (70, 80):     55,
                (80, 90):     25,
                (90, 100):    19,
                (100, 110):   12,
                (110, 120):   13,
                (120, 130):   8,
                (130, 140):   12,
                (140, 150):   4,
                (150, 160):   7,
                (160, 170):   3,
                (170, 180):   4,
                (180, 190):   4,
                (190, 200):   4,
                (200, 210):   3,
                (210, 220):   5,
                (220, 230):   4,
                (230, 240):   3,
                (240, 250):   5,
                (250, 260):   5,
                (260, 270):   2,
                (270, 280):   1,
                (310, 320):   2,
                (320, 330):   1,
                (340, 350):   1,
                (360, 370):   1,
                (470, 480):   1,
                (520, 530):   1,
                (670, 680):   1,
                (830, 840):   1,
                (880, 890):   1,
                (1040, 1050): 1,
            }
        }
        self.log_analyzer.do_cmd("ip-time-to-render --filter '^68.171.231.81$' --step 10")
        self.assertEqual(self.log_analyzer._hpibttrl.aggr_data, expected_data)

if __name__ == '__main__':
    TESTDIR = os.path.dirname(os.path.abspath(__file__))
    MODULEDIR = os.path.dirname(TESTDIR)
    TOPDIR = os.path.dirname(MODULEDIR)
    sys.path.append(TOPDIR)
    
    from loganalyze.loganalyze import *

    enabled_test_cases = (
        October24th2011_AppServer06_Port8131,
    )

    suite = unittest.TestSuite()
    for test in enabled_test_cases:
        tmp = unittest.TestLoader().loadTestsFromTestCase(test)
        suite.addTest(tmp)
    unittest.TextTestRunner(verbosity=2).run(suite)

else: 

    from loganalyze.loganalyze import *
